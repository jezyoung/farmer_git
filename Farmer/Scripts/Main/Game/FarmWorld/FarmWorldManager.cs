using Farmer.Scripts.Main.Game.Collectibles;
using Farmer.Scripts.Main.Game.Database;
using Farmer.Scripts.Main.Game.FarmWorld.Characters;
using Farmer.Scripts.Main.Game.FarmWorld.Farming.Crops;
using Farmer.Scripts.Main.Game.FarmWorld.TileMaps;
using Godot;

namespace Farmer.Scripts.Main.Game.FarmWorld
{
    public class FarmWorldManager: Node2D
    {
        [Export()] private NodePath _tileMapManagerNodePath;
        [Export()] private NodePath _cropsManagerNodePath; // temp, replace for class that keeps track of plants that are planted
        [Export()] private NodePath _collectiblesManagerNodePath;
        
        public TileMapManager tileMapManager;
        public CropsManager cropsManager;
        public CollectiblesManager collectiblesManager;

        [Signal]
        private delegate void ItemConsumed();

        public override void _Ready()
        {
            tileMapManager = GetNode<Node2D>(_tileMapManagerNodePath) as TileMapManager;
            cropsManager = GetNode<Node2D>(_cropsManagerNodePath) as CropsManager;
            collectiblesManager = GetNode<Node2D>(_collectiblesManagerNodePath) as CollectiblesManager;
        }
        
        // TEMP
        public void InteractWithWorld(Vector2 mousePos, ItemResource item)
        {
            // check for tool or seed selected in inventory and for distance and select action: all done outside of this class
            Vector2 mapPosition = tileMapManager.MouseToMapPosition(mousePos);
            Vector2 gridPosition = tileMapManager.MouseToGridPosition(mousePos);
            // GD.Print(mapPosition);
            

            if (item.itemName == "Shovel")
            {
                tileMapManager.farmSoilTileMap.AddSoilTile(mapPosition);
            }
            else if (item.itemName == "Seed" && tileMapManager.farmSoilTileMap.CheckForFarmSoil(mapPosition))
            {
                if (item.stackAmount > 0)
                {
                    cropsManager.PlantCrop(gridPosition, item.cropPackedScene);
                    EmitSignal(nameof(ItemConsumed), item);
                }
               
            }
        }
    }
}