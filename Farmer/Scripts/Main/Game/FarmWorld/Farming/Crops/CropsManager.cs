using Godot;

namespace Farmer.Scripts.Main.Game.FarmWorld.Farming.Crops
{
    public class CropsManager: Node2D
    {
        [Export] private PackedScene _cropPackedScene; // TEMP

        public void PlantCrop(Vector2 cropPosition, PackedScene cropPackedScene)
        {
            if (cropPackedScene == null)
            {
                GD.Print("CropsManager: cropPackedScene is null");
                return;
            }
            var crop = cropPackedScene.Instance() as Node2D;
            AddChild(crop);
            if(crop != null ) crop.Position = cropPosition;
        }

        public void UpdateCropsGrowth()
        {
            foreach (Node2D cropNode in GetChildren())
            {
                var crop = cropNode as Crop;
                crop?.GrowPlant();
            }
        }
    }
}