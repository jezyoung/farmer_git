using Godot;

namespace Farmer.Scripts.Main.Game.FarmWorld.Farming.Crops
{
    public class Crop: Node2D
    {
        [Export()] private NodePath _cropSpriteNodePath;
        private Sprite _cropSprite;

        private float _growStage = 0;

        public override void _Ready()
        {
            _cropSprite = GetNode<Sprite>(_cropSpriteNodePath);
        }

        public void GrowPlant()
        {
            _growStage = _growStage + 16;
            _cropSprite.RegionRect = new Rect2(new Vector2(_growStage,0), new Vector2(16,16) );
        }
    }
}