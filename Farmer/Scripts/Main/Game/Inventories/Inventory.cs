using Farmer.Scripts.Main.Game.Database;
using Farmer.Scripts.Main.Game.GameUI.InventoryUI;
using Godot;
using Godot.Collections;

namespace Farmer.Scripts.Main.Game.Inventories
{
    public class Inventory : Node2D
    {
        [Export()] protected NodePath _inventoryUiNodePath;
        [Export()] protected int _slotAmount = 30;
        [Export()] public InventoryType inventoryType;
        
        protected InventoryUi _inventoryUi;
        protected ItemResource _selectedSlotItem;

        public Dictionary<int, ItemResource> inventory = new Dictionary<int, ItemResource>();
        public ItemResource SelectedSlotItem => _selectedSlotItem;
        public int InventoryId { get; set; }

        [Signal]
        protected delegate void InventoryChanged();

        [Signal]
        protected delegate void MouseEnteredUiSlot();
        
        
        public override void _Ready()
        {
            _inventoryUi = GetNode<Control>(_inventoryUiNodePath) as InventoryUi;
            _inventoryUi.uiSlots = new InventorySlot[_slotAmount];
            _inventoryUi.Connect("MouseEnteredSlot", this, nameof(OnMouseEnteredUiSlot));
            // GD.Print(_inventoryUi.uiSlots.Length);
            // populate inventory with empty items
            for (int i = 0; i < (_slotAmount); i++)
            {
                inventory[i] = new ItemResource();
            }

            _selectedSlotItem = inventory[0];
        }



        protected void OnMouseEnteredUiSlot(int slotIndex)
        {
            if (inventory[slotIndex].itemName != null)
            {
                _selectedSlotItem = inventory[slotIndex]; 
                // GD.Print("slot has item");
            }
            else
            {
                _selectedSlotItem = new ItemResource(); 
                // GD.Print("slot is empty");
            }
            
          
            
            EmitSignal(nameof(MouseEnteredUiSlot), slotIndex, InventoryId, inventoryType);
        }

        public virtual void UpdateInventoryUi()
        {
            for (int j = 0; j < _slotAmount; j++)
            {
                // _inventoryUi.slots[j] = inventory[j];
                // slots[j] = inventory[j];
                // if (slots[j].itemName != null)
                // {
                //     _inventoryUI.AddItemToUISlot(j, slots[j], slots[j].stackAmount);
                // }
                _inventoryUi.AddItemToUiSlot(j, inventory[j], inventory[j].stackAmount);
                
            }
        }

        
        public void MoveItem(ItemResource item, int index)
        {
            inventory[index] = item;
            _selectedSlotItem = item;
            // GD.Print(inventory[index].itemName);
            // CallDeferred(nameof(UpdateInventoryUi));
        }
        

        public void ItemConsumed(ItemResource item)
        {
            for (int i = 0; i < inventory.Count; i++)
            {
                if (inventory[i].itemId == item.itemId)
                {
                    if (inventory[i].stackable)
                    {
                        if (inventory[i].stackAmount > 0)
                        {
                            --inventory[i].stackAmount;
                            
                            if (inventory[i].stackAmount <= 0)
                            {
                                inventory[i] = new ItemResource();
                            }
                        }
                        else inventory[i] = new ItemResource();
                        
                    }
                    else inventory[i] = new ItemResource();

                    break;
                }
            }
            // CallDeferred(nameof(UpdateInventoryUi));
        }

        public void RemoveItem(int index)
        {
            inventory[index] = new ItemResource();
            // CallDeferred(nameof(UpdateInventoryUi));
        }

      

        protected bool InventoryContains(int id)
        {
            bool result = false;
            for (int i = 0; i < inventory.Count; i++)
            {
                result = inventory[i].itemId == id;
                if (result) break;
            }

            return result;
        }
    }
}