using Farmer.Scripts.Main.Game.Database;
using Farmer.Scripts.Main.Game.GameUI.InventoryUI;
using Godot;

namespace Farmer.Scripts.Main.Game.Inventories
{
    public class PlayerInventory: Inventory
    {
        [Export()]private NodePath _hotbarUiNodePath;
        private HotbarUi _hotbarUi;

        public ItemResource hotbarSlotItem;
        
        public override void _Ready()
        {
            base._Ready();
            
            _hotbarUi = GetNode<Control>(_hotbarUiNodePath) as HotbarUi;
            _hotbarUi.Connect("HotbarSlotSelected", this, nameof(OnHotbarSlotSelected));
            // TEMP
            
            for (int i = 0; i < ItemDatabase.Instance.items.Count; i++)
            {
                if (ItemDatabase.Instance.items[i].itemName == "Shovel")
                {
                    AddItem(ItemDatabase.Instance.items[i].itemId);
                }
            }
           
        }
        
        public void OnUseSelectedItem(int index)
        {
            // GD.Print("use " + inventory[index].itemName);
            // _selectedSlotItem = inventory[index];
        }
        
        public override void _Process(float delta)
        {
            // TEMP
            if (Input.IsActionJustPressed("ui_inventory"))
            {
                _inventoryUi.Visible = !_inventoryUi.Visible;
            }
        }

        public override void UpdateInventoryUi()
        {
            base.UpdateInventoryUi();
            for (int i = 0; i < _hotbarUi.uiHotbarSlots.Length; i++)
            {
                _hotbarUi.AddItemToUiSlot(i, inventory[i],inventory[i].stackAmount);
            }
        }

        public void AddItem(int id)
        {
            ItemResource itemToAdd = new ItemResource();

            while (itemToAdd.itemName == null)
            {
                for (int k = 0; k < ItemDatabase.Instance.items.Count; k++)
                {
                    if (ItemDatabase.Instance.items[k].itemId == id) // find the item in the database 
                    {
                        itemToAdd = new ItemResource();
                        itemToAdd.itemId = ItemDatabase.Instance.items[k].itemId;
                        itemToAdd.itemName = ItemDatabase.Instance.items[k].itemName;
                        itemToAdd.stackable = ItemDatabase.Instance.items[k].stackable;
                        itemToAdd.iconTexture = ItemDatabase.Instance.items[k].iconTexture;
                        itemToAdd.itemType = ItemDatabase.Instance.items[k].itemType;
                        itemToAdd.stackAmount = ItemDatabase.Instance.items[k].stackAmount;
                        itemToAdd.maxStackSize = ItemDatabase.Instance.items[k].maxStackSize;
                        itemToAdd.cropPackedScene = ItemDatabase.Instance.items[k].cropPackedScene;
                    }
                }

                break;
            }
            

            for (var i = 0; i < inventory.Count; i++)
            {
                if (i > inventory.Count)
                {
                    GD.Print("Inventory full");
                    return;
                }

                if (inventory[i].itemId != itemToAdd.itemId || !itemToAdd.stackable) continue;
                if (inventory[i].stackAmount >= inventory[i].maxStackSize) continue;
                ++inventory[i].stackAmount;
                EmitSignal(nameof(InventoryChanged));
                CallDeferred(nameof(UpdateInventoryUi));
                return;
            }

            for (var i = 0; i < inventory.Count; i++)
            {
                if (i > inventory.Count)
                {
                    GD.Print("Inventory full");
                    return;
                }


                if (inventory[i].itemName != null) continue;
                // GD.Print(  itemToAdd.itemName+ " not found in inventory and added as new");
                inventory[i] = itemToAdd; // add to inventory
                EmitSignal(nameof(Inventory.InventoryChanged));
                CallDeferred(nameof(UpdateInventoryUi));
                return;
            }
            
        }

        void OnHotbarSlotSelected(int index)
        {
            if (inventory[index] == null)
            {
                hotbarSlotItem = new ItemResource();
                hotbarSlotItem.itemName = "";
            }
            else
            {
                hotbarSlotItem = inventory[index];
                
            }
            
          
            
        }
    }
}