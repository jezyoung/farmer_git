using Farmer.Scripts.Main.Game.Database;
using Farmer.Scripts.Main.Game.GameUI.InventoryUI;
using Godot;

namespace Farmer.Scripts.Main.Game.Inventories
{
    public class InventoryManager : Control
    {
        // for now one inventory, when all refactored, make player inventory, chests etc

        private Inventory[] _inventories;
        private InventoryType _inventoryType;
        private bool _itemInSlot;
        private bool _itemPickedUpForMove;
        private ItemResource _itemToMove;
        private bool _leftMouseButtonPressed;
        private MouseMoveItem _mouseMoveItemIcon;
        [Export] private PackedScene _mouseMoveItemIconPackedScene;
        private CanvasLayer _mouseMoveItemLayer;
        private int _mouseOnSlotIndex;
        public PlayerInventory playerInventory;
        private int _selectedInventory;
        private int _swapIndex;

        public override void _Ready()
        {
            _inventories = new Inventory[GetChildCount()];

            for (var i = 0; i < GetChildren().Count; i++)
            {
                var inventory = GetChild(i) as Inventory;
                _inventories[i] = inventory;
                _inventories[i].InventoryId = i;
                _inventories[i].Connect("MouseEnteredUiSlot", this, nameof(OnMouseEnteredUiSlot));
            }

            for (var i = 0; i < _inventories.Length; i++)
                if (_inventories[i].inventoryType == InventoryType.PlayerInventory)
                    playerInventory = _inventories[i] as PlayerInventory;
                else GD.PushWarning("PlayerInventory not found!");
            _mouseMoveItemLayer = new CanvasLayer();
            _mouseMoveItemLayer.Layer = 2;
            AddChild(_mouseMoveItemLayer);
            _mouseMoveItemIcon = _mouseMoveItemIconPackedScene.Instance() as MouseMoveItem;
            _mouseMoveItemLayer.AddChild(_mouseMoveItemIcon);
            // _mouseMoveItemIcon.ShowOnTop = true;

            _itemToMove = new ItemResource();
        }


        public override void _Process(float delta)
        {
            // GD.Print(_mouseOnSlot + " " + _selectedInventory + " " + _inventoryType);

            if (_itemPickedUpForMove)
                _mouseMoveItemIcon.ShowItemMouseIcon(_itemToMove.iconTexture, _itemToMove.stackAmount,
                    GetGlobalMousePosition());
            else
                _mouseMoveItemIcon.HideItemMouseIcon();

            
        }

        private bool ItemInSlot()
        {
            return _inventories[_selectedInventory].SelectedSlotItem.itemName != null;
        }

        public override void _Input(InputEvent @event)
        {

            if (@event is InputEventMouseButton eventMouseButton)
            {
                if (eventMouseButton.ButtonIndex == 1) // TODO move 1 item
                {
                    if (eventMouseButton.Pressed)
                    {
                        _leftMouseButtonPressed = true;

                         if(_itemPickedUpForMove) // if there is an item picked up for move
                        {
                            if(ItemInSlot())  // if there is already an item in the selected slot
                            {
                                var itemToSwap = _inventories[_selectedInventory].SelectedSlotItem;
                                

                                if (itemToSwap.itemId != _itemToMove.itemId) // if it is not the same item
                                {
                                    _inventories[_selectedInventory].MoveItem(_itemToMove, _mouseOnSlotIndex);
                                    _itemToMove = itemToSwap;
                                    _itemPickedUpForMove = true;
                                }
                                else if (_itemToMove.stackable) // if the item is stackable
                                {
                                    if (_swapIndex != _mouseOnSlotIndex)
                                    {
                                        GD.Print("item comes from other index");
                                        _inventories[_selectedInventory].SelectedSlotItem.stackAmount =
                                            _itemToMove.stackAmount + _inventories[_selectedInventory]
                                                .SelectedSlotItem.stackAmount;
                                        _itemToMove = new ItemResource();
                                        _itemPickedUpForMove = false;
                                    }
                                    else
                                    {
                                        GD.Print("item comes from same index");
                                        // return;
                                    }
                                    
                                }
                            }
                            else // if there is no item in selected slot
                            {
                                _inventories[_selectedInventory].MoveItem(_itemToMove, _mouseOnSlotIndex);
                                _itemToMove = new ItemResource();
                                _itemPickedUpForMove = false;
                                return;
                            }
                           
                        }
                        
                        else // if no item is picked up for move
                        {
                            if (ItemInSlot()) // if there is already an item in the selected slot
                            {
                                _itemToMove = _inventories[_selectedInventory].SelectedSlotItem;
                                
                                
                                if(_itemToMove.stackable )
                                {
                                    if (_itemToMove.stackAmount >= 2)
                                    {
                                        --_inventories[_selectedInventory].SelectedSlotItem.stackAmount;
                                        _itemToMove = new ItemResource();
                                        _itemToMove.stackable = true;
                                        _itemToMove.iconTexture = _inventories[_selectedInventory].SelectedSlotItem
                                            .iconTexture;
                                        _itemToMove.itemId = _inventories[_selectedInventory].SelectedSlotItem.itemId;
                                        _itemToMove.itemName =
                                            _inventories[_selectedInventory].SelectedSlotItem.itemName;
                                        _itemToMove.itemType =
                                            _inventories[_selectedInventory].SelectedSlotItem.itemType;
                                        _itemToMove.stackAmount = 1;
                                        _itemToMove.maxStackSize = _inventories[_selectedInventory].SelectedSlotItem
                                            .maxStackSize;
                                        _itemToMove.cropPackedScene = _inventories[_selectedInventory].SelectedSlotItem
                                            .cropPackedScene;
                                    }
                                    else
                                    {
                                        _inventories[_selectedInventory].RemoveItem(_mouseOnSlotIndex);
                                        
                                    }
                                    
                                }
                                else 
                                {
                                    _inventories[_selectedInventory].RemoveItem(_mouseOnSlotIndex);
                                    
                                }
                                
                                _itemPickedUpForMove = true;
                                _swapIndex = _mouseOnSlotIndex;
                            }
                        }

                       
                    }
                    else
                    {
                        _leftMouseButtonPressed = false;
                    }
                }

                else if (eventMouseButton.ButtonIndex == 2) // TODO move full stack
                    if (eventMouseButton.Pressed)
                        
                        if (!_itemPickedUpForMove)
                        {
                            if (ItemInSlot()) // if there is already an item in the selected slot
                            {
                                _itemToMove = _inventories[_selectedInventory].SelectedSlotItem;


                                if (_itemToMove.stackable)
                                {
                                    _inventories[_selectedInventory].RemoveItem(_mouseOnSlotIndex);
                                    _itemPickedUpForMove = true;
                                    _swapIndex = _mouseOnSlotIndex;
                                }
                            }
                        }
            }

            _inventories[_selectedInventory].UpdateInventoryUi();
        }

        private void OnMouseEnteredUiSlot(int slotIndex, int inventoryId, InventoryType inventoryType)
        {
            // GD.Print("mouseEntered");
            _mouseOnSlotIndex = slotIndex;
            _selectedInventory = inventoryId;
            _inventoryType = inventoryType;
            // GD.Print("Mouse entered slot: " + slotIndex + " in inventory " + inventoryType);
        }


        public void AddToPlayerInventory(int id)
        {
            playerInventory.AddItem(id);
        }
    }
}