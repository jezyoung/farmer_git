using Farmer.Scripts.Main.Game.Database;
using Farmer.Scripts.Main.Game.FarmWorld;
using Farmer.Scripts.Main.Game.FarmWorld.Characters;
using Farmer.Scripts.Main.Game.GameUI;
using Farmer.Scripts.Main.Game.GameUI.InventoryUI;
using Farmer.Scripts.Main.Game.Inventories;
using Farmer.Scripts.Main.Game.TimeManagement;
using Godot;

namespace Farmer.Scripts.Main.Game
{
    public class GameManager: Node2D
    {
        [Export()] private NodePath _farmWorldManagerNodePath; // replace for packedscene and instantiate
        [Export()] private NodePath _playerNodePath;
        [Export()] private NodePath _mouseSelectorNodePath;
        [Export()] private NodePath _mouseControllerNodePath;
        [Export()] private NodePath _inventoryManagerNodePath;
        [Export()] private NodePath _dayCycleNodePath;
        
        public FarmWorldManager farmWorldManager;
        public Player player;
        public MouseSelector mouseSelector;
        public MouseController mouseController;

        private DayCycle _dayCycle;
        private InventoryManager _inventoryManager;
        private string _selectedTool = "Hoe"; // replace for enum or state

        public override void _Ready()
        {
            farmWorldManager = GetNode<Node2D>(_farmWorldManagerNodePath) as FarmWorldManager;
            player = GetNode<KinematicBody2D>(_playerNodePath) as Player;
            mouseSelector = GetNode<Node2D>(_mouseSelectorNodePath) as MouseSelector;
            mouseController = GetNode<Node2D>(_mouseControllerNodePath) as MouseController;
            _inventoryManager = GetNode<Control>(_inventoryManagerNodePath) as InventoryManager;
            _dayCycle = GetNode<Node2D>(_dayCycleNodePath) as DayCycle;

            mouseController.Connect("MouseMotionEvent", this, nameof(OnMouseMotionEvent));
            mouseController.Connect("LeftMouseButtonEvent", this, nameof(OnLeftMouseButtonEvent));
            // _inventoryUI.Connect("InventoryUIButtonPressed", this, nameof(OnInventoryUIButtonPressed));
            _dayCycle.Connect("DayPassedEvent", this, nameof(OnDayPassedEvent));
            farmWorldManager.collectiblesManager.Connect("PlayerPickedUpCollectible", this,
                nameof(OnPlayerPickedUpCollectible));
            farmWorldManager.Connect("ItemConsumed", this, nameof(OnItemConsumed));
        }

        void OnMouseMotionEvent(Vector2 mousePos)
        {
            // GD.Print("gamemanager: on mouse motion");
            // GD.Print(FarmWorldManager.TileMapManager.MouseToPlayerPixelDistance(mousePos, Player.Position));
        }

        void OnLeftMouseButtonEvent(Vector2 mousePos)
        {
            // GD.Print("GameManager: OnLeftMouseButtonEvent");
            
            // check for tool in inventory and for distance and select action: all done outside of this class
            if (farmWorldManager.tileMapManager.MouseToPlayerPixelDistance(mousePos, player.Position) <= 23)
            {
                if (_inventoryManager.playerInventory.hotbarSlotItem.itemName != null)
                {
                    farmWorldManager.InteractWithWorld(mousePos, _inventoryManager.playerInventory.hotbarSlotItem);
                    GD.Print(_inventoryManager.playerInventory.hotbarSlotItem);
                }
                
               
            }
            
        }

        // void OnInventoryUIButtonPressed(string tool)
        // {
        //     _selectedTool = tool;
        // }

        void OnDayPassedEvent()
        {
            farmWorldManager.cropsManager.UpdateCropsGrowth();
        }

        void OnPlayerPickedUpCollectible(int id)
        {
            // GD.Print("player picked up collectible " + ItemDatabase.Instance.GetItem(id).itemName);
            _inventoryManager.AddToPlayerInventory(id);
        }

        void OnItemConsumed(ItemResource item)
        {
            // GD.Print(item.itemName + " consumed");
            _inventoryManager.playerInventory.ItemConsumed(item);
            // _inventory.ItemConsumed(item);
        }
    }
}