using Farmer.Scripts.Main.Game.Database;
using Godot;
using Godot.Collections;

namespace Farmer.Scripts.Main.Game.Collectibles
{
    public class CollectiblesManager: Node2D
    {
        [Signal]
        private delegate void PlayerPickedUpCollectible();
       
        void OnPlayerPickedUpCollectible(int id)
        {
            
            EmitSignal(nameof(PlayerPickedUpCollectible), id);
        }
        
    }
}