using Farmer.Scripts.Main.Game.Database;
using Godot;

namespace Farmer.Scripts.Main.Game.GameUI.InventoryUI
{
    public class InventoryUiMouseController: Control
    {
        private bool _leftMouseButtonPressed = false;
        private bool _draggingItem = false;
        private ItemResource _draggedItem;
        
        public override void _GuiInput(InputEvent @event)
        {
            if (@event is InputEventMouseButton eventMouseButton)
            {
                if (eventMouseButton.ButtonIndex == 1)
                {
                    if (eventMouseButton.Pressed)
                    {
                        // _leftMouseButtonPressed = true;
                        // _selectedSlot = uiSlots[_mouseOnSlot];
                        // EmitSignal(nameof(UseSelectedItem), _selectedSlot.slotIndex);
                        // GD.Print("selected slot " + _selectedSlot.slotIndex);
                    }
                    else
                    {
                        _leftMouseButtonPressed = false;
                    }
                }
               
                
            }
            
            if (@event is InputEventMouseMotion eventMouseMotion)
            {
                if (eventMouseMotion.Speed > Vector2.Zero && _leftMouseButtonPressed && !_draggingItem)
                {
                    
                    // _draggingItem = true;
                    // _draggedItem = slots[_mouseOnSlot];
                    // slots[_mouseOnSlot] = new ItemResource();
                    // GD.Print("dragging item: " + _draggedItem.itemName);
                }

            }
            
        }
    }
}