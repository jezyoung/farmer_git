using Farmer.Scripts.Main.Game.Database;
using Godot;
using Godot.Collections;

namespace Farmer.Scripts.Main.Game.GameUI.InventoryUI
{
    public class InventoryUi: Control
    {
        [Export()] private NodePath _slotParentNodePath;
        [Export()] private PackedScene _slotPackedScene;
        private GridContainer _slotParent;

        public InventorySlot[] uiSlots;

        [Signal]
        private delegate void MouseEnteredSlot();

        public override void _Ready()
        {
            _slotParent = GetNode<GridContainer>(_slotParentNodePath);
        }

        void _on_Inventory_ready()
        {
            for (int i = 0; i < uiSlots.Length; i++)
            {
                var uiSlot = _slotPackedScene.Instance() as InventorySlot;
                _slotParent.AddChild(uiSlot);
                uiSlot.Connect("MouseEnteredSlot", this, nameof(OnMouseEnteredSlot));
                uiSlots[i] = uiSlot;
                uiSlot.slotIndex = i;
            }
        }

        public void AddItemToUiSlot(int index, ItemResource item, int stackAmount)
        {
            // if (item.iconTexture == null) return;
            uiSlots[index].ChangeIcon(item.iconTexture);
            uiSlots[index].UpdateStackAmountLabel(stackAmount);
        }

        void OnMouseEnteredSlot(int index)
        {
            EmitSignal(nameof(MouseEnteredSlot), index);
        }

    }
}